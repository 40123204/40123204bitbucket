import hashlib
import string
import random
 
''' sha1 secure hashes '''
 
# convert user_password into sha1 encoded string
def gen_password(user_password):
    return hashlib.sha1(user_password.encode("utf-8")).hexdigest()
 
# generate random user password
def user_password(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))
 
# read lines from file
lines = open("2b_stud_list.txt", "r", encoding="utf-8").read().splitlines()
 
# we may also need to notice every user with computer generated passwords
for i in range(len(lines)):
    password = user_password(8, "ABCDEFGHJKMNPQRSTUVWXYZ23456789")
    print(lines[i].split("\t")[0], "-", password, "-", gen_password(password), \
            "-", lines[i].split("\t")[1])