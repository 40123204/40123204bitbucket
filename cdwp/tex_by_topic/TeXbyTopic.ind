\begin{theindex}

  \item \char94\char94\ replacement, \hyperpage{33}
  \item \n{tfm} files, \hyperpage{55}
  \item {\tt  
    \subitem }character 
      \subsubitem escape, \hyperpage{35}
  \item {\tt  dvi} file, \hyperpage{263}
  \item \char 126, \hyperpage{187}

  \indexspace

  \item accents, \hyperpage{48}
  \item accents in math mode, \hyperpage{195}
  \item active character, \hyperpage{117}
  \item alignment tab, \hyperpage{219}
  \item alignments, \hyperpage{217--224}
    \subitem rules in, \hyperpage{222--223}
  \item arguments, \hyperpage{111}
  \item arithmetic, \hyperpage{83}
    \subitem  on glue, \hyperindexformat{\see{glue, arithmetic on}}{90}
  \item assignment
    \subitem box size, \hyperpage{65}
    \subitem font, \hyperpage{56}
    \subitem global, \hyperpage{56}, \hyperpage{65}, \hyperpage{106}
    \subitem local, \hyperpage{106}

  \indexspace

  \item badness, \hyperpage{94}
    \subitem  and line breaking, \hyperpage{176}
    \subitem  calculation, \hyperpage{94}
  \item baseline
    \subitem distance, \hyperpage{155}
  \item box
    \subitem bounding, \hyperpage{56}
  \item boxes, \hyperpage{59--72}
  \item boxes 
    \subitem text in, \hyperpage{69}
  \item braces, \hyperpage{107--108}
    \subitem explicit, \hyperpage{106}
    \subitem implicit, \hyperpage{106}
  \item breakpoints
    \subitem computation of, \hyperpage{230--231}
  \item breakpoints in math lists, \hyperpage{208}
  \item breakpoints in vertical lists, \hyperpage{229}

  \indexspace

  \item category
    \subitem 0, \hyperpage{30}, \hyperpage{32}, \hyperpage{129}
    \subitem 1, \hyperpage{30}, \hyperpage{67}, \hyperpage{106}, 
		\hyperpage{110}
    \subitem 2, \hyperpage{30}, \hyperpage{67}, \hyperpage{106, 107}, 
		\hyperpage{110}
    \subitem 3, \hyperpage{30}, \hyperpage{202}, \hyperpage{211}
    \subitem 4, \hyperpage{30}, \hyperpage{219}
    \subitem 5, \hyperpage{30}, \hyperpage{40}
    \subitem 6, \hyperpage{30}, \hyperpage{32}, \hyperpage{111}
    \subitem 7, \hyperpage{30}, \hyperpage{33}, \hyperpage{203}
    \subitem 8, \hyperpage{31}, \hyperpage{203}
    \subitem 9, \hyperpage{31}
    \subitem 10, \hyperpage{31, 32}, \hyperpage{34}, \hyperpage{39}, 
		\hyperpage{52}, \hyperpage{55}, \hyperpage{114}, 
		\hyperpage{132}
    \subitem 11, \hyperpage{31, 32}, \hyperpage{34}, \hyperpage{48}, 
		\hyperpage{80}, \hyperpage{143}
    \subitem 12, \hyperpage{31}, \hyperpage{39}, \hyperpage{48}, 
		\hyperpage{52}, \hyperpage{55}, \hyperpage{80}, 
		\hyperpage{83}, \hyperpage{85}, \hyperpage{92}, 
		\hyperpage{114}, \hyperpage{132}, \hyperpage{142, 143}, 
		\hyperpage{270}
    \subitem 13, \hyperpage{31}, \hyperpage{117}, \hyperpage{280}
    \subitem 14, \hyperpage{31}
    \subitem 15, \hyperpage{31}, \hyperpage{33}
    \subitem 16, \hyperpage{31}, \hyperpage{49}, \hyperpage{140}
  \item category code, \hyperpage{30}
  \item character
    \subitem codes, \hyperpage{45}
    \subitem escape, \hyperpage{33}
    \subitem extendable, \hyperpage{194}
    \subitem hyphen, \hyperpage{180}
    \subitem implicit, \hyperpage{47}
    \subitem parameter, \hyperpage{34}, \hyperpage{114}
    \subitem space, \hyperpage{37}
  \item character 
    \subitem active, and \cs{noexpand}, \hyperpage{129}
  \item code
    \subitem lowercase, \hyperindexformat{\see{lowercase, code}}{50}
    \subitem uppercase, \hyperindexformat{\see{uppercase, code}}{50}
  \item codenames, \hyperpage{51}
  \item command 
    \subitem primitive, \hyperpage{117}
  \item commands
    \subitem horizontal, \hyperpage{74}
    \subitem vertical, \hyperpage{75}
  \item Computer Modern, \hyperpage{266}
  \item conditionals, \hyperpage{139}
    \subitem evaluation of, \hyperpage{144}
  \item control
    \subitem sequence, \hyperpage{32}
    \subitem symbol, \hyperpage{32}
  \item cramped styles, \hyperpage{202}
  \item current page, \hyperpage{228}

  \indexspace

  \item date, \hyperpage{264}
  \item delimiter
    \subitem size, \hyperpage{193--194}
  \item delimiter code, \hyperpage{193}
  \item delimiters, \hyperpage{192--194}
  \item demerits, \hyperpage{177}
  \item device driver, \hyperpage{264}
  \item device drivers, \hyperpage{265}
  \item discardable items, \hyperpage{74}
  \item discretionary hyphen, \hyperpage{180}
  \item discretionary item, \hyperpage{179}
  \item display alignment, \hyperpage{218}
  \item display math, \hyperpage{211}
  \item displays
    \subitem non-centred, \hyperpage{214}

  \indexspace

  \item equation numbering, \hyperpage{213--214}
  \item error patching, \hyperpage{275}
  \item expansion, \hyperpage{125}
    \subitem expandable constructs, \hyperpage{125}
  \item extension font, \hyperpage{209}

  \indexspace

  \item files, \hyperpage{245}
  \item fixed-point
    \subitem arithmetic, \hyperpage{84}
  \item floating-point
    \subitem arithmetic, \hyperpage{84}
  \item font
    \subitem dimensions, \hyperpage{55}
  \item font families, \hyperpage{197}
  \item font files, \hyperpage{265}
  \item font metrics, \hyperpage{264}
  \item font tables, \hyperpage{300}
  \item fonts, \hyperpage{53}
  \item format file, \hyperpage{259}
  \item formula
    \subitem axis   of, \hyperpage{205}
    \subitem centring of, \hyperpage{205}
  \item frenchspacing, \hyperpage{188}

  \indexspace

  \item generalized fractions, \hyperpage{207}
  \item glue, \hyperpage{87}
    \subitem arithmetic on, \hyperpage{90}
    \subitem interline, \hyperpage{155}
    \subitem setting, \hyperpage{94}
    \subitem shrink     component of, \hyperpage{93}
    \subitem stretch component of, \hyperpage{93}
  \item group
    \subitem delimiters, \hyperpage{106}
  \item grouping, \hyperpage{105}

  \indexspace

  \item horizontal alignment, \hyperpage{218}
  \item hyphenation, \hyperpage{181}

  \indexspace

  \item I/O
    \subitem asynchronous, \hyperpage{247}
    \subitem file, \hyperpage{245--249}
    \subitem screen, \hyperpage{248}
  \item indentation
    \subitem hanging, \hyperpage{170--171}
  \item \csname IniTeX\endcsname , \hyperpage{259}
  \item input
    \subitem stack, \hyperpage{118}
  \item input files, \hyperpage{245}
  \item insertions, \hyperpage{241}
  \item integer, \hyperpage{79}
  \item italic correction, \hyperpage{56}

  \indexspace

  \item job, \hyperpage{255--256}

  \indexspace

  \item kerning, \hyperpage{56}
  \item keywords, \hyperpage{280}

  \indexspace

  \item language, \hyperpage{182}
    \subitem current, \hyperpage{182}
  \item languages, \hyperpage{180}
  \item \csname LaTeX\endcsname , \hyperpage{262}
  \item leaders, \hyperpage{101}
    \subitem rule, \hyperpage{101}
  \item ligatures, \hyperpage{57}
  \item line
    \subitem  end, \hyperpage{29}
    \subitem  input, \hyperpage{29}
    \subitem empty, \hyperpage{36}
    \subitem terminator, \hyperpage{40}
    \subitem width, \hyperpage{170}
  \item line breaking, \hyperpage{175--183}
    \subitem badness, \hyperpage{176}
  \item list
    \subitem horizontal, \hyperpage{73}
    \subitem vertical, \hyperpage{74}
  \item lists 
    \subitem horizontal
      \subsubitem  breakpoints in, \hyperpage{177}
  \item log file, \hyperpage{256}
  \item Lollipop, \hyperpage{262}
  \item lowercase
    \subitem code, \hyperpage{50}

  \indexspace

  \item machine dependence, \hyperpage{213}
  \item machine independence, \hyperpage{29}
  \item macro, \hyperpage{109}
    \subitem definition, \hyperpage{110}
    \subitem outer, \hyperpage{110}
  \item magnification, \hyperpage{263}
  \item marks, \hyperpage{237}
  \item math characters, \hyperpage{192}
  \item math classes, \hyperpage{204}
  \item math mode, \hyperpage{202}
    \subitem display, \hyperpage{202}
    \subitem non-display, \hyperpage{202}
  \item math shift character, \hyperpage{202}
  \item math spacing, \hyperpage{205--207}
  \item math styles, \hyperpage{202}
  \item math symbols, lists of, \hyperpage{305}
  \item math units, \hyperpage{205}
  \item migrating material, \hyperpage{77}
  \item mode, \hyperpage{73}
    \subitem horizontal, \hyperpage{73}
    \subitem internal vertical, \hyperpage{75}
    \subitem restricted horizontal, \hyperpage{75}
    \subitem vertical, \hyperpage{74}
  \item mu glue, \hyperpage{206}

  \indexspace

  \item number
    \subitem conversion, \hyperpage{82}
  \item numbers, \hyperpage{79}

  \indexspace

  \item output routine, \hyperpage{235--240}
  \item overflow errors, \hyperpage{276--278}

  \indexspace

  \item page
    \subitem breaking, \hyperpage{227--234}
    \subitem builder, \hyperpage{228}
    \subitem depth, \hyperpage{226}
    \subitem height, \hyperpage{226}
    \subitem length, \hyperpage{228--229}
    \subitem numbering, \hyperpage{238}
  \item page positioning, \hyperpage{225}
  \item paragraph
    \subitem breaking into lines, \hyperpage{175--183}
    \subitem end, \hyperpage{165--168}
    \subitem shape, \hyperpage{169--174}
    \subitem start, \hyperpage{159--163}
  \item parameter, \hyperpage{111}
    \subitem character, \hyperpage{114}
    \subitem delimited, \hyperpage{112}
    \subitem undelimited, \hyperpage{112}
  \item Pascal, \hyperpage{266}
  \item penalties
    \subitem in vertical mode, \hyperpage{229}
  \item penalties in math mode, \hyperpage{208}
  \item point
    \subitem scaled, \hyperpage{82}
  \item PostScript, \hyperpage{264}
  \item prefixes 
    \subitem macro, \hyperpage{110}
  \item primitive commands, \hyperpage{117}

  \indexspace

  \item quad, \hyperpage{205}

  \indexspace

  \item radical, \hyperpage{194}
  \item recent contributions, \hyperpage{228}
  \item recursion, \hyperpage{118}
  \item registers
    \subitem allocation of, \hyperpage{251--252}
  \item roman numerals, \hyperpage{83}
  \item rules, \hyperpage{99}
  \item run modes, \hyperpage{256--257}

  \indexspace

  \item save stack, \hyperpage{105}
  \item shrink, \hyperpage{93}
  \item slant
    \subitem per point, \hyperpage{49}
  \item space
    \subitem  token, \hyperpage{39}
    \subitem control, \hyperpage{32}, \hyperpage{187}
    \subitem factor, \hyperpage{186}
    \subitem optional, \hyperpage{280}
  \item spacefactor code, \hyperpage{188}
  \item spaces
    \subitem funny, \hyperpage{39}
    \subitem optional, \hyperpage{37}
  \item spacing, \hyperpage{185--189}
  \item specials, \hyperpage{264}
  \item states
    \subitem internal, \hyperpage{32}
  \item statistics, \hyperpage{269}
  \item streams, \hyperpage{246}
  \item stretch, \hyperpage{93}
  \item subscript, \hyperpage{203}
  \item successor, \hyperpage{194}
  \item superscript, \hyperpage{203}
  \item symbol font, \hyperpage{208}

  \indexspace

  \item table, character codes, \hyperpage{299}
  \item table, {\sc  ascii}, \hyperpage{298}
  \item tables, \hyperpage{217}
  \item tables, font, \hyperpage{300}
  \item \csname TeX\endcsname , \hyperpage{259}
  \item \csname TeX\endcsname , big, \hyperpage{277}
  \item \csname TeX\endcsname , version 2, \hyperpage{281}
  \item \csname TeX\endcsname , version 3, \hyperpage{67}, 
		\hyperpage{182}, \hyperpage{244}, \hyperpage{281}
  \item tie, \hyperpage{187}
  \item time, \hyperpage{264}
  \item token
    \subitem list, \hyperpage{151}
    \subitem space, \hyperpage{37}
  \item tracing, \hyperpage{269--273}
  \item TUG, \hyperpage{267}
  \item TUGboat, \hyperpage{267}

  \indexspace

  \item units of measurement, \hyperpage{91}
  \item uppercase
    \subitem code, \hyperpage{50}

  \indexspace

  \item verbatim mode, \hyperpage{123}
  \item vertical alignment, \hyperpage{218}
  \item \csname VirTeX\endcsname , \hyperpage{259}
  \item virtual fonts, \hyperpage{265}

  \indexspace

  \item \csname web\endcsname , \hyperpage{266}
  \item whatsits, \hyperpage{247}

\end{theindex}
